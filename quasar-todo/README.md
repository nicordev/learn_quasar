# Quasar Todo (quasar-todo)

A Quasar Project

> [getting started playlist](https://www.youtube.com/playlist?list=PLFZAa7EupbB5r2oPtDzN0DY2D0jym9Ih_)

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
