# Timap (timap)

My timer app

## features

- [x] add trainings
  - [x] add rounds
- [ ] edit a training
- [ ] save trainings
- [ ] repeat a training
- [ ] add a warm up before the training start
- [ ] disable a round
- [ ] highlight the current round
- [ ] pause the current round
- [ ] resume the current round
- [ ] end the current round
- [ ] add a countdown for the current round
- [x] play a sound when a round ends
- [ ] set the end sound for each round
- [ ] add relevant default trainings
  - [ ] heart coherence
  - [ ] HIIT session
  - [ ] stretching session

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
