export default [
  {
    name: 'flash',
    rounds: [
      {
        id: 1,
        duration: 10,
        name: 'sautiller avec garde de boxe'
      },
      {
        id: 2,
        duration: 15,
        name: 'rafale de coups de poings directs'
      },
      {
        id: 3,
        duration: 30,
        name: 'squats avec coups de pieds latéraux'
      },
      {
        id: 4,
        duration: 15,
        name: 'esquives rotatives'
      },
      {
        id: 5,
        duration: 15,
        name: 'burpees'
      },
      {
        id: 6,
        duration: 30,
        name: 'gainage en remontant sur les bras'
      },
      {
        id: 7,
        duration: 30,
        name: 'pompes'
      }
    ]
  },
  {
    name: 'Cohérence cardiaque 365',
    rounds: [
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      },
      {
        id: 1,
        duration: 5,
        name: 'in'
      },
      {
        id: 2,
        duration: 5,
        name: 'out'
      }
    ]
  }
]
