# learn_quasar

## build native android app using capacitor

> [documentation](https://quasar.dev/quasar-cli-vite/developing-capacitor-apps/preparation)

build:

```sh
quasar build -m capacitor -T android
```

sign:

```sh
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 20000
```

```sh
~/Android/Sdk/build-tools/34.0.0/zipalign -v 4 pathToUnsignedApkFile HelloWorld.apk
```

```sh
~/Android/Sdk/build-tools/34.0.0/apksigner sign --ks my-release-key.keystore --ks-key-alias alias_name HelloWorld.apk
```

## css classes

col-xs-9
- begin small first

## q-list

```
<q-list>
    <q-item>
        <q-item-section>
            Hello world
        </q-item-section>
        <q-item-section side>
            Another section
        </q-item-section>
    </q-item>
</q-list>
```

use `q-item-section` inside `q-item` even for a single word.

side
- use only for the last item, like q-item-section

size
- sm for small
- for q-item-section for instance

## router

In a layout, the tag `<router-view />` will be replaced by children

```js
<router-view />
```

## layout

```sh
quasar new layout LayoutNameHere
```

## page

```sh
quasar new page PageNameHere
```

## colors

- [tutorial #6](https://www.youtube.com/watch?v=hLrWqyF0wxU&list=PLFZAa7EupbB5r2oPtDzN0DY2D0jym9Ih_&index=7)
- [theme builder](https://quasar.dev/style/theme-builder)

Get color code:

```html
<script>
import { defineComponent } from 'vue'
import { colors } from 'quasar'

const { getPaletteColor } = colors // to avoid loading all functions

export default defineComponent({
  mounted () {
    console.log(getPaletteColor('blue-6'))
  },
})
</script>
```

Style by file:

```html
<style lang="scss">
.page-bg {
  background-color: $blue-3;
}
</style>
```

## extract logic

- [tutorial #7](https://www.youtube.com/watch?v=mMnN4aKaPKI&list=PLFZAa7EupbB5r2oPtDzN0DY2D0jym9Ih_&index=7)

```sh
quasar new component ComponentNameHere
```

3 steps to use a component:

```html
<template>
  <q-page class="row">
    <div class="col-xs-3">
      <q-toolbar class="bg-primary">
        <!-- 1. Use the component. PascalCase is used as a convention for homemade components, though dash-case can be used too. -->
        <!-- Note: the formatting is passed to the component, so prefer format here than in the component file -->
        <!-- fab: floating button -->
        <CreateTodoButton
          fab
          round
          color="secondary"
          style="margin-bottom: -48px;"
          class="q-ml-lg"
          icon="mdi-plus"
        ></CreateTodoButton>
      </q-toolbar>
    </div>
  </q-page>
</template>

<script>
import { defineComponent } from 'vue'
// 2. import the component
import CreateTodoButton from 'components/CreateTodoButton.vue'

export default defineComponent({
  name: 'IndexPage',

  // 3. declare the component
  components: {
    CreateTodoButton
  },
})
</script>
```
Note: the formatting is passed to the component, so prefer format here than in the component file

## plugins

Declare plugins in `quasar.config.js`

```js
    // https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#framework
    framework: {
      // Quasar plugins
      plugins: [
        'Dialog',
        'Notify'
      ]
    },
```

Use plugins with `this.$q.pluginNameHere`

```html

<script>
export default {
  methods: {
    handleButtonClicked () {
      this.$q.dialog({
        title: 'Create Todo',
        prompt: {
          model: '',
          type: 'text'
        }
      }).onOk(this.createTodo)
    },
    createTodo (data) {
      this.$q.notify({
        message: 'Todo Created!',
        icon: 'mdi-check',
        color: 'positive'
      })
    }
  }
}
</script>
```

Using the composition API:

```html
<script>
import { useQuasar } from 'quasar'

const $q = useQuasar()

function handleButtonClicked () {
  $q.dialog({
    title: 'Create Todo',
    prompt: {
      model: '',
      type: 'text'
    }
  }).onOk(this.createTodo)
}

function createTodo (data) {
  $q.notify({
    message: 'Todo Created!',
    icon: 'mdi-check',
    color: 'positive'
  })
}
</script>
```
